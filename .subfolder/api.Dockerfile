FROM ruby:2.7-rc

RUN apt-get update -yqq \
  && apt-get install -yqq --no-install-recommends curl \
  && curl -sL https://deb.nodesource.com/setup_10.x | bash - \
  && curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | bash apt-key add - \
  && echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list

RUN apt-get update -yqq \
  && apt-get install -yqq --no-install-recommends build-essential nodejs yarn \
  && apt-get -q clean \
  && rm -rf /var/lib/apt/lists

# Pre-install gems with native extensions
RUN gem install nokogiri -v "1.10.7"

WORKDIR /usr/src/app
COPY Gemfile* ./

RUN bundle update --bundler
RUN bundle install
